from bs4 import BeautifulSoup
import requests


def main():
    root_url = 'https://www.rit.edu/programs/computing-security-bs'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    request = requests.get(root_url, headers=headers, timeout=10)
    soup = BeautifulSoup(request.text, 'html.parser')

    tables = soup.findChildren('table')

    courses = []
    for table in tables:
        rows = soup.findChildren(['tr'])
        for row in rows:
            cells = row.findChildren('td')
            for cell in cells:
                cell.string = cell.string.replace('\xa0', '')
            if(len(cells) == 3 and cells[0].string != '' and cells[1].string != ''):
                course = {'number': cells[0].string.strip(
                ), 'name': cells[1].string.strip()}
                courses.append(course)

    f = open('courses.csv', 'w')
    for course in courses:
        f.write('{},{},\n'.format(course['number'], course['name']))
    f.close()


if __name__ == '__main__':
    main()
