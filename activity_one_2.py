from bs4 import BeautifulSoup
from multiprocessing import Pool
import requests


def main():
    rit_root = 'https://www.rit.edu'
    root_url = 'https://www.rit.edu/gccis/computingsecurity/people'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    request = requests.get(root_url, headers=headers, timeout=10)
    soup = BeautifulSoup(request.text, 'html.parser')

    staff_pics = soup.findAll("div", {"class": "staff-picture"})
    pic_urls = []
    for staff in staff_pics:
        pic_urls.append(staff.find('img').get('src'))

    with Pool(10) as p:
        p.map(download_staff_pic, pic_urls)


def download_staff_pic(pic):
    pic_url = '{}{}'.format('https://www.rit.edu', pic)
    pic_len = len(pic.split('/'))
    file_name = pic.split('/')[pic_len - 1]
    with open('staff_pics/{}'.format(file_name), 'wb') as handle:
        request = requests.get(pic_url, stream=True)
        for block in request.iter_content(1024):
            if not block:
                break
            handle.write(block)


if __name__ == '__main__':
    main()
